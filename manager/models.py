from django.db import models


# Create your models here.
class Client(models.Model):
    name = models.CharField(max_length=45)
    lastname = models.CharField(max_length=45)
    observations = models.CharField(max_length=45)

    def __str__(self):
        return f"{self.name} {self.lastname}"


class Waiter(models.Model):
    name = models.CharField(max_length=45)
    lastname1 = models.CharField(max_length=45)
    lastname2 = models.CharField(max_length=45)

    def __str__(self):
        return f"{self.name} {self.lastname1} {self.lastname2}"


class Saucer(models.Model):
    name = models.CharField(max_length=45)
    amount = models.IntegerField()

    def __str__(self):
        return f"{self.name}"


class Drink(models.Model):
    name = models.CharField(max_length=45)
    amount = models.IntegerField()

    def __str__(self):
        return f"{self.name}"


class Table(models.Model):
    quantity_client = models.IntegerField()
    location = models.CharField(max_length=45)

    def __str__(self):
        return f"{self.location} {self.quantity_client}"


class Invoice(models.Model):
    date = models.DateTimeField(auto_now=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    waiter = models.ForeignKey(Waiter, on_delete=models.CASCADE)
    table = models.ForeignKey(Table, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.client}, {self.waiter}, table {self.table}, {self.date}"


class Order(models.Model):
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE)
    quantity_drink = models.IntegerField(blank=True, null=True)
    drink = models.ForeignKey(Drink, on_delete=models.CASCADE, blank=True, null=True)
    quantity_saucer = models.IntegerField(blank=True, null=True)
    saucer = models.ForeignKey(Saucer, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return f"{self.invoice}"
