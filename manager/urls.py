from django.urls import path
from .views import home
from .views import WaiterList, ClientList, SaucerList, DrinkList, TableList, InvoiceList, OrderList
from .views import WaiterDetail, ClientDetail, SaucerDetail, DrinkDetail, TableDetail, InvoiceDetail, OrderDetail

urlpatterns = [
    # Rest framework urls
    path('waiters/', WaiterList.as_view()),
    path('waiters/<int:pk>/', WaiterDetail.as_view()),
    path('clients/', ClientList.as_view()),
    path('clients/<int:pk>/', ClientDetail.as_view()),
    path('saucers/', SaucerList.as_view()),
    path('saucers/<int:pk>/', SaucerDetail.as_view()),
    path('drinks/', DrinkList.as_view()),
    path('drinks/<int:pk>/', DrinkDetail.as_view()),
    path('tables/', TableList.as_view()),
    path('tables/<int:pk>/', TableDetail.as_view()),
    path('invoices/', InvoiceList.as_view()),
    path('invoices/<int:pk>/', InvoiceDetail.as_view()),
    path('orders/', OrderList.as_view()),
    path('orders/<int:pk>/', OrderDetail.as_view()),
]
