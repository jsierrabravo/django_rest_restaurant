from .models import Client
from django import forms


class ClientForm(forms.Form):
    name = forms.CharField(max_length=45)
    lastname = forms.CharField(max_length=45)
    observations = forms.CharField(max_length=45)

# class CreateNewContact(forms.Form):
#     name = forms.CharField(label="Name", max_length=20, required=True)
#     email = forms.EmailField(label="Email", required=True)
#     phone_number = forms.IntegerField(label="Phone number", required=True)
