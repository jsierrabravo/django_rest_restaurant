from django.contrib import admin
from .models import Client, Waiter, Drink, Saucer, Table, Invoice, Order

# Register your models here.
admin.site.register(Client)
admin.site.register(Waiter)
admin.site.register(Drink)
admin.site.register(Saucer)
admin.site.register(Table)
admin.site.register(Invoice)
admin.site.register(Order)

